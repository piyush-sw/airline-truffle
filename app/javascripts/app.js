// Import the page's CSS. Webpack will know what to do with it.
//import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

// Import our contract artifacts and turn them into usable abstractions.
//import metacoin_artifacts from '../../build/contracts/MetaCoin.json'
import user_artifacts from '../../build/contracts/User.json'
import btr_artifacts from '../../build/contracts/BreathTest.json'

// MetaCoin is our usable abstraction, which we'll use through the code below.
//var MetaCoin = contract(metacoin_artifacts);
var User = contract(user_artifacts);
var BTR = contract(btr_artifacts);

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
var accounts;
var account;

window.App = {
    start: function () {
        var self = this;

        // Bootstrap the MetaCoin abstraction for Use.
        //MetaCoin.setProvider(web3.currentProvider);
        User.setProvider(web3.currentProvider);
        BTR.setProvider(web3.currentProvider);

        // Get the initial account balance so it can be displayed.
        web3.eth.getAccounts(function (err, accs) {
            if (err != null) {
                self.setStatus("There was an error fetching your accounts.");
                return;
            }

            if (accs.length == 0) {
                self.setStatus("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
                return;
            }

            accounts = accs;
            account = accounts[0];

            self.setStatus("");
            //self.refreshBalance();
        });

        // Check whether UserContract Address is already set, if not then set it.
        BTR.deployed().then(function (instance) {
            return instance.userContractAddress({from: account});
        }).then(function (address) {
            if (address == undefined || address == '' || address == '0x0000000000000000000000000000000000000000') {
                User.deployed().then(function (instance) {
                    return instance.address;
                }).then(function (address) {
                    self.setUserCtrAddress(address);
                }).catch(function (e) {
                    console.log(e);
                    self.setStatus("Error Fetching User data; see log.");
                });
            }
        }).catch(function (e) {
            console.log(e);
            self.setStatus("Error Fetching BTR  data; see log.");
        });

        //  Register global Watcher
        // var userInstance;
        //User.deployed().then(function (instance) {
        //    userInstance = instance;
        //
        //    var registrations = userInstance.allEvents({fromBlock: 0, toBlock: 'latest'});
        //
        //    registrations.watch(function (error, result) {
        //        // This will catch all Transfer events, regardless of how they originated.
        //        if (error == null) {
        //            console.log(result.args);
        //        } else {
        //            self.setStatus("Error Watching Events. See Console for logs.");
        //            console.log(error);
        //        }
        //    });
        //});
    },

    setStatus: function (message) {
        var status = document.getElementById("status");
        if (message == undefined || message.trim() == '') {
            document.getElementById("alert").style.display = 'none';
        } else {
            document.getElementById("alert").style.display = 'block';
        }
        status.innerHTML = message;
    },

    //refreshBalance: function () {
    //    var self = this;
    //
    //    var meta;
    //    MetaCoin.deployed().then(function (instance) {
    //        meta = instance;
    //        return meta.getBalance.call(account, {from: account});
    //    }).then(function (value) {
    //        var balance_element = document.getElementById("balance");
    //        balance_element.innerHTML = value.valueOf();
    //    }).catch(function (e) {
    //        console.log(e);
    //        self.setStatus("Error getting balance; see log.");
    //    });
    //},

    //sendCoin: function () {
    //    var self = this;
    //
    //    var amount = parseInt(document.getElementById("amount").value);
    //    var receiver = document.getElementById("receiver").value;
    //
    //    this.setStatus("Initiating transaction... (please wait)");
    //
    //    var meta;
    //    MetaCoin.deployed().then(function (instance) {
    //        meta = instance;
    //        return meta.sendCoin(receiver, amount, {from: account});
    //    }).then(function () {
    //        self.setStatus("Transaction complete!");
    //        self.refreshBalance();
    //    }).catch(function (e) {
    //        console.log(e);
    //        self.setStatus("Error sending coin; see log.");
    //    });
    //},

    registerUser: function () {
        var self = this;

        var username = document.getElementById("username").value;

        this.setStatus("Registering... (please wait)");

        var userInstance;
        User.deployed().then(function (instance) {
            userInstance = instance;
            return userInstance.registerUser(username, {from: account});
        }).then(function () {
            self.setStatus("User Registered!");
        }).catch(function (e) {
            console.log(e);
            self.setStatus("Error Registering User; see log.");
        });
    },

    deRegisterUser: function () {
        var self = this;

        var username = document.getElementById("dUsername").value;

        this.setStatus("De-Registering... (please wait)");

        var userInstance;
        User.deployed().then(function (instance) {
            userInstance = instance;
            return userInstance.unsubscribe(username, {from: account});
        }).then(function () {
            self.setStatus("User De-Registered!");
        }).catch(function (e) {
            console.log(e);
            self.setStatus("Error Registering User; see log.");
        });
    },

    getUserData: function () {
        var self = this;

        var username = document.getElementById("username2").value;

        this.setStatus("Fetching... (please wait)");

        var userInstance;
        User.deployed().then(function (instance) {
            userInstance = instance;
            return userInstance.getData(username, {from: account});
        }).then(function (data) {
            self.setStatus("User Data: " + data);
        }).catch(function (e) {
            console.log(e);
            self.setStatus("Error Fetching User data; see log.");
        });
    },

    setUserCtrAddress: function (userAddress) {
        var self = this;

        this.setStatus("Setting User Contract Address... (please wait)");

        var btrInstance;
        BTR.deployed().then(function (instance) {
            btrInstance = instance;
            return btrInstance.setUserContractAddress(userAddress, {from: account});
        }).then(function (data) {
            self.setStatus("User Address set." + data);
        }).catch(function (e) {
            console.log(e);
            self.setStatus("Error Setting UserContract Address; see log.");
        });
    },

    setBTR: function () {
        var self = this;

        var username = document.getElementById("username3").value;
        var amt = document.getElementById("amt").value;

        this.setStatus("Setting BTR... (please wait)");

        var btrInstance;
        BTR.deployed().then(function (instance) {
            btrInstance = instance;
            return btrInstance.setBreathTestResults(username, amt, {from: account});
        }).then(function (data) {
            self.setStatus("BTR  set." + data);
        }).catch(function (e) {
            console.log(e);
            self.setStatus("Error Setting BTR; see log.");
        });
    },

    //showLogsInTable: function (logs) {
    //    var status = document.getElementById("events");
    //    var text = "<ul>";
    //    for (var i = 0; i < logs.length; i++) {
    //        text += "<li>" + Object.keys(logs[i].args)[0] + " : " + logs[i].args[Object.keys(logs[i].args)[0]] + "</li>";
    //    }
    //    text += "</ul>";
    //    status.innerHTML = text;
    //},

    size: function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    },

    showLogsInTable: function (logs, header, body) {
        var self = this;
        if (logs.length != 0) {
            var numberOfColumns = self.size(Object.keys(logs[0].args));

            var headerElement = '<tr><th>#</th>';
            var bodyElements = '';
            for (var i = 0; i < logs.length; i++) {
                bodyElements += '<tr><td>' + (i + 1) + '</td>';

                for (var j = 0; j < numberOfColumns; j++) {
                    if (i == 0) {   // process header only once
                        headerElement += "<th>" + Object.keys(logs[i].args)[j] + "</th>";
                    }
                    bodyElements += "<td>" + logs[i].args[Object.keys(logs[i].args)[j]] + "</td>";
                }
                bodyElements += '</tr>';
            }
            headerElement += '</tr>';

            header.innerHTML = headerElement;
            body.innerHTML = bodyElements;
        } else {
            body.innerHTML = '<tr><td>No Transactions to display</td>';
        }

        self.setStatus("");
    },

    listAllTransactions: function () {
        var self = this;

        self.setStatus("Listing all Transactions ... (please wait)");

        var userInstance;
        User.deployed().then(function (instance) {
            userInstance = instance;
            var registrations = userInstance.UserRegistered(null, {fromBlock: 0, toBlock: 'latest'});
            //registrations = userInstance.allEvents({fromBlock: 0, toBlock: 'latest'});

            // This one is for individual Events (Would get called when actual transaction is performed)
            //registrations.watch(function (error, result) {
            //    // This will catch all Transfer events, regardless of how they originated.
            //    if (error == null) {
            //        console.log(result.args);
            //    } else {
            //        self.setStatus("Error Watching Events. See Console for Error");
            //        console.log(error);
            //    }
            //});

            // would get all past logs again.
            registrations.get(function (error, logs) {
                if (!error) {
                    //console.log(logs);
                    self.showLogsInTable(logs, document.getElementById("user-table-header"), document.getElementById("user-table-body"));
                } else {
                    self.setStatus("Error getting Events. See Console for error");
                    console.log(error);
                }
            });

            // would stop and uninstall the filter
            registrations.stopWatching();


            var btrFilter = userInstance.BTRSet(null, {fromBlock: 0, toBlock: 'latest'});
            btrFilter.get(function (error, logs) {
                if (!error) {
                    //console.log(logs);
                    self.showLogsInTable(logs, document.getElementById("btr-table-header"), document.getElementById("btr-table-body"));
                } else {
                    self.setStatus("Error getting Events. See Console for error");
                    console.log(error);
                }
            });

            // would stop and uninstall the filter
            btrFilter.stopWatching();

            var deRegisterFilter = userInstance.UserDeRegistered(null, {fromBlock: 0, toBlock: 'latest'});
            deRegisterFilter.get(function (error, logs) {
                if (!error) {
                    //console.log(logs);
                    self.showLogsInTable(logs, document.getElementById("dUser-table-header"), document.getElementById("dUser-table-body"));
                } else {
                    self.setStatus("Error getting Events. See Console for error");
                    console.log(error);
                }
            });

            // would stop and uninstall the filter
            deRegisterFilter.stopWatching();
        });

    }
};

window.addEventListener('load', function () {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof web3 !== 'undefined') {
        console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
        // Use Mist/MetaMask's provider
        window.web3 = new Web3(web3.currentProvider);
    } else {
        console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
        // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
        window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    }

    App.start();
});
