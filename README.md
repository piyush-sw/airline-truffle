# POC Blockchain Application.
An Dapp that records user's BreathTest on Blockchain and based on the results allow/dis-allows certain operations.

# Background
Airline is highly regulated industry and there is no scope for any errors. There are many processes that are already in practice but all practices may not be followed strictly because they might not be automated and digitized.

There are few things that are done manually and manual work is prone to errors. 
Also, tracking the fault is more difficult.

# Idea
Based on our research, we've identified few scenarios where we're using Blockchain and IOT to automate and digitize them.

One such scenario is to synchronize the Pilot's breath test results with Flight control.
As part of regulation, every pilot has to take breath test and Pilot is not allowed to fly under influence of alcohol.
Situation:
Currently, Pilot can bypass the test and forge the results. Also, such incidents are identified after multiple occurrences. Moreover investigation is a cumbersome process done by setting up a committee and manually verifying all the records.

# Solution:
We're using IOT and blockchain to get and store pilot's breath test results. Putting them on blockchain ensures that data is immutable and can be backtracked anytime. So consensus and trust both are achieved. 
These results are verified by Flight control to allow a pilot to fly. 
Or they can directly accessed from flight and block Pilot even from starting the engines.

Other scenario is where Pilots in training has to log their flight times.
Situation:
Pilots are manually doing it (Maybe on a paper-sheet), which brings concerns mentioned above including authenticity.
Solution:
Our product will automatically store the Flight start time and flight stop times on blockchain that addresses all the concerns.

These are just two scenarios we've considered and started with. There will be many.


P.S.
We haven't validated these scenarios "legally" yet with any Airline industry. 
This is just an POC application built on top of Ethereum Blockchain.