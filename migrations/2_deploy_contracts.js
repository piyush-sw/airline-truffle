//var ConvertLib = artifacts.require("./ConvertLib.sol");
//var MetaCoin = artifacts.require("./MetaCoin.sol");
var Owned = artifacts.require("./Owned.sol");
var Mortal = artifacts.require("./Mortal");
var User = artifacts.require("./User.sol");
var BreathTest = artifacts.require("./BreathTest.sol");
var FlightStartLog = artifacts.require("./FlightStartLog.sol");

module.exports = function (deployer) {
    //deployer.deploy(ConvertLib);
    //deployer.link(ConvertLib, MetaCoin);
    //deployer.deploy(MetaCoin);

    deployer.deploy(Owned);
    deployer.deploy(Mortal);

    deployer.deploy(User);

    deployer.deploy(BreathTest);

    //deployer.deploy(FlightStartLog);
};
