pragma solidity ^0.4.2;


// import "./Std.sol";
import "./User.sol";


/**
 * BreathTest Provider
 */
contract BreathTest is Mortal {

    // public so as to get the Address
    address public userContractAddress;

    /**
     * TODO: onlyowner is removed for now. Introduce it later again.
    */
    function setUserContractAddress(address _userContractAddress) {
        userContractAddress = _userContractAddress;
    }

    /**
     * This function will set the BreathTest results for a given User
     * Function access is protected by onlyowner modifier so that transaction instance only can access it.
     * TODO: onlyowner is removed for now. Introduce it later again.
     */
    function setBreathTestResults(string _username, int _amt) {
        if (userContractAddress == address(0)) {
            throw;
        }
        // User contrct not asoociated
        User pilot = User(userContractAddress);
        // pilot object will get all functionality of UserContract
        pilot.setData(_username, _amt);
    }

}
