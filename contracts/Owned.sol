pragma solidity ^0.4.2;


contract Owned {

    address owner;

    function owned() {
        owner = msg.sender;
    }

    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    modifier onlyowner() {
        if (msg.sender == owner) _;
    }
}
