pragma solidity ^0.4.2;


import "./Mortal.sol";


contract User is Mortal {
    string public userName;

    event UserRegistered(string _username, uint _registeredOn); // so you can log the event
    event UserDeRegistered(string _username, uint _deRegisteredOn); // so you can log the event
    event BTRSet(string _username, int _testResult, uint _testedOn); // so you can log the event

    address btrProvider;

    mapping (string => Service) services; // Each provider will have their own address hence Map key is Address for that user.

    struct Service {// Would be BreathTest, FlightStartTime
    bool active;
    uint lastUpdate;
    int data;      // BreathTest result, flightStartTime timestamp
    }


//Function will link BTR Provider to User Contract Instance
    function registerToProvider(address _providerAddress) onlyowner {
        btrProvider = _providerAddress;
    }

    function registerUser(string _username) {
        services[_username] = Service({
        active : true,
        lastUpdate : now,
        data : - 1
        });

        if (services[_username].active == false) {
            throw;
        }

        UserRegistered(_username, now);
    }

/**
 * User is allowed to unsubscribe from the Provider with specific address
 */
    function unsubscribe(string _username) {
        if (services[_username].active) {// Guarding condition can be changed to be more realistic
            services[_username].active = false;
            UserDeRegistered(_username, now);
        }
        else {
            throw;
        }
    }


/**
 * THis function sets the state variable value.
 * TODO: Verify whether blocks are created for each
 * Also, sender's record is changed so User contract instance will not be able to use this function.
 * Everything is public in ethereum hence User contract instance can see and call the function.
 * Calling it would fail anyway.
 *
 * THIS FUNCTION IS EXAMPLE OF TWO CONTRACTS COMMUNICATING WITH EACH OTHER.
 */
    function setData(string _username, int _data) {
        if (services[_username].active) {
            services[_username].lastUpdate = now;
            services[_username].data = _data;

            BTRSet(_username, _data, now);
        }
        else {
            throw;
        }
    }

    function getData(string _username) constant returns (int) {
        if (services[_username].active) {
            return services[_username].data;
        }
        else {
            throw;
        }
    }
}
